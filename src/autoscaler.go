package src

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"github.com/machinebox/graphql"
	v1 "k8s.io/api/core/v1"
	k8sclient "k8s.io/client-go/kubernetes"
)

type AutoScalerConfig struct {
	Name         string         `yaml:"name"`
	Tags         []string       `yaml:"tags"`
	TargetQueue  int            `yaml:"target_queue"`
	BaseCapacity int            `yaml:"base_capacity"`
	Runners      []RunnerConfig `yaml:"runners"`
}

type RunnerConfig struct {
	Name        string `yaml:"name"`
	RunnerToken bool   `yaml:"runner_token"`
	Deployment  string `yaml:"deployment"`
	Namespace   string `yaml:"namespace"`
	Concurrent  int    `yaml:"concurrent"`
	Min         int    `yaml:"min"`
	Max         int    `yaml:"max"`
}

type RunnerData struct {
	Id      int
	Name    string
	Paused  bool
	Running int
}

type RunnerRef struct {
	running *RunnerData
	paused  *RunnerData
	scale   int
}

func runner_data(k8s *k8sclient.Clientset, gl *graphql.Client, ctx context.Context, cfg RunnerConfig, tags []string) ([]*RunnerData, error) {
	pods, err := pods_in_deployment(k8s, ctx, cfg.Namespace, cfg.Deployment)
	if err != nil {
		return nil, err
	}

	var active_runners = 0
	for _, pod := range pods.Items {
		if pod.Status.Phase == v1.PodRunning && pod.DeletionTimestamp == nil {
			active_runners++
		}
	}

	if active_runners > cfg.Max {
		return nil, errors.Errorf("%s has %d active runners > max %d", cfg.Name, active_runners, cfg.Max)
	}

	var idx = 0
	var data = make([]*RunnerData, len(pods.Items))
	for _, p := range pods.Items {
		if p.Status.Phase == v1.PodFailed || p.Status.Phase == v1.PodSucceeded {
			continue
		}
		var runner_name = p.Name
		if cfg.RunnerToken {
			runner_name = cfg.Name
		}
		id, err := runner_by_name(gl, ctx, runner_name)
		if err != nil {
			if errors.Is(err, err_no_matching_runners_found) {
				if p.Status.Phase == v1.PodPending {
					// pod not yet started
					continue
				}
				if p.DeletionTimestamp != nil {
					// pod being terminated
					continue
				}
			}
			return nil, err
		}
		runner, err := runner_details(gl, ctx, id)
		if err != nil {
			return nil, err
		}
		if !is_equivalent(runner.TagList, tags) {
			return nil, errors.Errorf("runner %s has wrong tags", runner_name)
		}
		jobs, err := runner_jobcount(gl, ctx, id)
		if err != nil {
			return nil, err
		}
		data[idx] = &RunnerData{id, p.Name, runner.Paused, jobs}
		idx++
	}
	return data[:idx], nil
}

func _run_autoscaler(fnc *fnci, clients *Clients, cfg AutoScalerConfig) error {
	fmt.Println("")
	fmt.Println("runner:", cfg.Name)

	var data = make([][]*RunnerData, len(cfg.Runners))

	for idx, runner := range cfg.Runners {
		_data, err := fnc.runner_data(clients.K8S, clients.GL, clients.CTX, runner, cfg.Tags)
		if err != nil {
			return err
		}
		data[idx] = _data
	}

	for idx, runner := range cfg.Runners {
		var _data []*RunnerData
		for _, x := range data[idx] {
			if x.Paused && x.Running == 0 {
				marked, _, err := get_mark(clients.K8S, clients.CTX, runner.Namespace, x.Name, "controller.gitlab-runner-orchestrator.io/deleted")
				if err != nil {
					return err
				}
				if marked {
					fmt.Printf("already marked for deletion pod %s\n", x.Name)
				} else {
					fmt.Printf("deleting pod %s\n", x.Name)

					err := fnc.mark_pod_for_deletion(clients.K8S, clients.CTX, runner.Namespace, x.Name)
					if err != nil {
						return err
					}

					err = fnc.scale_down_deployment(clients.K8S, clients.CTX, runner.Namespace, runner.Deployment)
					if err != nil {
						return err
					}
				}
			} else if !x.Paused && x.Running == 0 {
				fmt.Printf("pausing %s\n", x.Name)
				err := fnc.pause(clients.GL, clients.CTX, x.Id)
				if err != nil {
					return err
				}
				x.Paused = true
				_data = append(_data, x)
			} else {
				_data = append(_data, x)
			}
		}
		data[idx] = _data
	}

	pending, err := fnc.pending_per_tag(clients.GL, clients.CTX, cfg.Tags)
	if err != nil {
		return err
	}
	var running_locally = 0
	var capacity = 0
	for idx, runner := range cfg.Runners {
		for _, x := range data[idx] {
			running_locally += x.Running
			if !x.Paused {
				capacity += runner.Concurrent
			} else {
				capacity += x.Running
			}
		}
	}

	if cfg.BaseCapacity > 0 {
		running_in_total, err := fnc.running_per_tag(clients.GL, clients.CTX, cfg.Tags)
		if err != nil {
			return err
		}
		//if running_locally > running_in_total {
		//	return errors.New("our local number of jobs is larger than the total number of jobs")
		//}
		free_base_capacity := cfg.BaseCapacity - (running_in_total - running_locally)
		pending = pending - free_base_capacity
	}

	fmt.Println("running / capacity:", running_locally, capacity)
	fmt.Println("pending:", pending)

	must_scale_up := func(capacity int) bool {
		return running_locally+pending > capacity+cfg.TargetQueue
	}
	must_scale_down := func(capacity int) bool {
		return running_locally+pending <= capacity
	}
	can_scale_down := func(capacity int, concurrent int) bool {
		return running_locally+pending <= capacity-concurrent
	}

	if must_scale_up(capacity) {
		fmt.Println("trying to scale up by unpausing runners")
	outer:
		for idx, runner := range cfg.Runners {
			for _, x := range data[idx] {
				if x.Paused {
					fmt.Printf("unpausing %s\n", x.Name)
					err := fnc.unpause(clients.GL, clients.CTX, x.Id)
					if err != nil {
						return err
					}
					capacity += runner.Concurrent
					if !must_scale_up(capacity) {
						break outer
					}
				}
			}
		}

		if must_scale_up(capacity) {
			fmt.Println("trying to scale up by creating new runners")
		outer2:
			for idx, runner := range cfg.Runners {
				for jdx := len(data[idx]); jdx < cfg.Runners[idx].Max; jdx++ {
					fmt.Printf("scaling up %s\n", runner.Deployment)
					err := fnc.scale_up_deployment(clients.K8S, clients.CTX, runner.Namespace, runner.Deployment)
					if err != nil {
						return err
					}

					if runner.RunnerToken {
						// when registered with a runner token, after scale up,
						// the runner will still remain paused.
						fmt.Printf("unpausing %s\n", runner.Name)
						id, err := runner_by_name(clients.GL, clients.CTX, runner.Name)
						if err != nil {
							return err
						}
						err = fnc.unpause(clients.GL, clients.CTX, id)
						if err != nil {
							return err
						}
					}

					capacity += runner.Concurrent
					if !must_scale_up(capacity) {
						break outer2
					}
				}
			}
		}

		if must_scale_up(capacity) {
			fmt.Println("can't scale up further")
		}
	} else if must_scale_down(capacity) {
		fmt.Println("trying to scale down by pausing runners")
	outer3:
		for idx := len(cfg.Runners) - 1; idx >= 0; idx-- {
			for jdx := len(data[idx]) - 1; jdx >= cfg.Runners[idx].Min; jdx-- {
				x := data[idx][jdx]
				if can_scale_down(capacity, cfg.Runners[idx].Concurrent) && !x.Paused {
					fmt.Printf("scaling down %s\n", cfg.Runners[idx].Name)
					err := fnc.pause(clients.GL, clients.CTX, x.Id)
					if err != nil {
						return err
					}
					capacity -= cfg.Runners[idx].Concurrent
					if !must_scale_down(capacity) {
						break outer3
					}
				}
			}
		}
		if must_scale_down(capacity) {
			fmt.Println("can't scale down further")
		}
	}

	return nil
}

type fnci struct {
	scale_up_deployment   func(client *k8sclient.Clientset, ctx context.Context, namespace string, name string) error
	scale_down_deployment func(client *k8sclient.Clientset, ctx context.Context, namespace string, name string) error
	mark_pod_for_deletion func(client *k8sclient.Clientset, ctx context.Context, namespace string, name string) error
	runner_data           func(k8s *k8sclient.Clientset, gl *graphql.Client, ctx context.Context, cfg RunnerConfig, tags []string) ([]*RunnerData, error)
	pause                 func(client *graphql.Client, ctx context.Context, runner_id int) error
	unpause               func(client *graphql.Client, ctx context.Context, runner_id int) error
	pending_per_tag       func(client *graphql.Client, ctx context.Context, tags []string) (int, error)
	running_per_tag       func(client *graphql.Client, ctx context.Context, tags []string) (int, error)
}

var fnc = fnci{
	scale_up_deployment:   scale_up_deployment,
	scale_down_deployment: scale_down_deployment,
	mark_pod_for_deletion: mark_pod_for_deletion,
	runner_data:           runner_data,
	pause:                 pause,
	unpause:               unpause,
	pending_per_tag:       pending_per_tag,
	running_per_tag:       running_per_tag,
}

func VerifyConfig(cfgs []AutoScalerConfig) error {
	for _, cfg := range cfgs {
		for _, runner := range cfg.Runners {
			if runner.RunnerToken && runner.Max > 1 {
				// we currently do not support more than one instance per runner if using runner-token,
				// as sub-runners cannot be paused individually
				// https://gitlab.com/gitlab-org/gitlab/-/issues/422972
				return errors.Errorf("%s is registered using runner-token but configures max==%d", runner.Name, runner.Max)
			}
		}
	}
	return nil
}

func RunAutoscaler(clients *Clients, cfg []AutoScalerConfig) error {

	var anyerror error
	for _, x := range cfg {
		err := _run_autoscaler(&fnc, clients, x)
		if err != nil {
			anyerror = err
		}
	}
	return anyerror
}
