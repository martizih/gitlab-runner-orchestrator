package src

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/machinebox/graphql"
	"github.com/pkg/errors"
	v1 "k8s.io/api/core/v1"
	k8serr "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sclient "k8s.io/client-go/kubernetes"
)

type ReaperConfig struct {
	Name               string   `yaml:"name"`
	DeletionDelayS     int      `yaml:"deletion_delay_s"`
	SilenceThresholdS  int      `yaml:"silence_threshold_s"`
	IgnoreGitlabStatus []string `yaml:"ignore_gitlab_status"`
}

func killable_based_on_job_log(client *k8sclient.Clientset, ctx context.Context, pod *v1.Pod) (bool, error) {
	if pod.Status.Phase == "Pending" {
		return false, nil
	}

	podLogOptions := &v1.PodLogOptions{
		Container: "build",
	}
	podLog, err := client.CoreV1().Pods(pod.Namespace).GetLogs(pod.Name, podLogOptions).Stream(ctx)
	if err != nil {
		return false, errors.Wrap(err, "failed to get pod log")
	}
	defer podLog.Close()

	return match_string_at_beginning_of_line(podLog, `{"command_exit_code":`, 1024)
}

func killable_based_on_no_new_output(client *k8sclient.Clientset, ctx context.Context, pod *v1.Pod, threshold time.Duration) (bool, error) {
	if pod.Status.Phase == "Pending" {
		return false, nil
	}
	creationTime := pod.ObjectMeta.CreationTimestamp.Time
	if time.Since(creationTime) < threshold {
		return false, nil
	}

	since := time.Now().Add(-threshold)
	podLogOptions := &v1.PodLogOptions{
		Container: "build",
		SinceTime: &metav1.Time{Time: since},
	}
	podLog, err := client.CoreV1().Pods(pod.Namespace).GetLogs(pod.Name, podLogOptions).Stream(ctx)
	if err != nil {
		return false, errors.Wrap(err, "failed to get pod log")
	}
	defer podLog.Close()

	return is_empty(podLog)
}

func killable_based_on_gitlab_status(client *graphql.Client, ctx context.Context, pod *v1.Pod, ignored_status []string) (bool, error) {
	_project_id := pod.Annotations["project.runner.gitlab.com/id"]
	_job_id := pod.Annotations["job.runner.gitlab.com/id"]
	if _project_id == "" || _job_id == "" {
		return false, errors.New("pod is missing necessary annotations")
	}
	job_id, err := strconv.Atoi(_job_id)
	if err != nil {
		return false, errors.Wrap(err, "failed to convert job_id to int")
	}
	project_id, err := strconv.Atoi(_project_id)
	if err != nil {
		return false, errors.Wrap(err, "failed to convert project_id to int")
	}
	full_path, err := project_by_id(client, ctx, project_id)
	if err != nil {
		return false, err
	}
	status, err := job_status(client, ctx, full_path, job_id)
	if err != nil {
		return false, err
	}
	return contains([]string{"SUCCESS", "FAILED", "CANCELED"}, status) && !contains(ignored_status, status), nil
}

func kill_pod_if_exists(client *k8sclient.Clientset, ctx context.Context, namespace string, name string) error {
	err := kill_pod(client, ctx, namespace, name)
	if err != nil {
		var oerr *k8serr.StatusError
		if errors.As(err, &oerr) && oerr.ErrStatus.Code == 404 {
			fmt.Println("pod already gone:", name)
			return nil
		} else {
			return err
		}
	}
	fmt.Println("killed:", name)
	return nil
}

func kill_after_delay(client *k8sclient.Clientset, ctx context.Context, namespace string, name string, delay_s int, async_err chan error) {
	go func() {
		time.Sleep(time.Duration(delay_s) * time.Second)
		err := kill_pod_if_exists(client, ctx, namespace, name)
		if err != nil {
			async_err <- err
		}
	}()
}

func _run_reaper(clients *Clients, cfg ReaperConfig, async_err chan error) error {
	pods, err := clients.K8S.CoreV1().Pods(cfg.Name).List(clients.CTX, metav1.ListOptions{})
	if err != nil {
		return errors.Wrap(err, "failed to list pods")
	}

	for _, p := range pods.Items {
		if !strings.HasPrefix(p.Name, "runner-") {
			// not a job
			continue
		}

		pod, err := clients.K8S.CoreV1().Pods(cfg.Name).Get(clients.CTX, p.Name, metav1.GetOptions{})
		if err != nil {
			// job is already gone
			continue
		}

		killable, err1 := killable_based_on_job_log(clients.K8S, clients.CTX, pod)
		if err1 == nil && killable {
			fmt.Println("must kill based on job log:", pod.Name)
			kill_after_delay(clients.K8S, clients.CTX, pod.Namespace, pod.Name, cfg.DeletionDelayS, async_err)
			continue
		}

		killable, err2 := killable_based_on_gitlab_status(clients.GL, clients.CTX, pod, cfg.IgnoreGitlabStatus)
		if err2 == nil && killable {
			fmt.Println("must kill based on gitlab status:", pod.Name)
			err = kill_pod_if_exists(clients.K8S, clients.CTX, pod.Namespace, pod.Name)
			if err != nil {
				return err
			}
		}

		threshold := time.Duration(cfg.SilenceThresholdS) * time.Second
		killable, err3 := killable_based_on_no_new_output(clients.K8S, clients.CTX, pod, threshold)
		if err3 == nil && killable {
			fmt.Printf("must kill as the job did not produce any output in the last %s: %s\n", threshold.String(), p.Name)
			err = kill_pod_if_exists(clients.K8S, clients.CTX, pod.Namespace, pod.Name)
			if err != nil {
				return err
			}
		}

		if err1 != nil || err2 != nil || err3 != nil {
			_, err = clients.K8S.CoreV1().Pods(cfg.Name).Get(clients.CTX, p.Name, metav1.GetOptions{})
			if err != nil {
				// job is already gone
				continue
			} else if err1 != nil {
				return err1
			} else if err2 != nil {
				return err2
			} else if err3 != nil {
				return err3
			} else {
				return errors.New("unreachable code")
			}
		}
	}
	return nil
}

func RunReaper(clients *Clients, cfg []ReaperConfig, async_err chan error) error {
	var anyerror error
	for _, x := range cfg {
		err := _run_reaper(clients, x, async_err)
		if err != nil {
			anyerror = err
		}
	}
	return anyerror
}
