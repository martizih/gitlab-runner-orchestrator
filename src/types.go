package src

import (
	"context"

	"github.com/machinebox/graphql"
	k8sclient "k8s.io/client-go/kubernetes"
)

type Clients struct {
	K8S *k8sclient.Clientset
	GL  *graphql.Client
	CTX context.Context
}
