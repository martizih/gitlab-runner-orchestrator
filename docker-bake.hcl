variable "CI_REGISTRY_IMAGE" {
  default = ""
}
variable "CI_DEFAULT_BRANCH" {
  default = ""
}

variable "CI_COMMIT_REF_SLUG" {
  default = ""
}

variable "BUILD_HASH" {
  default = ""
}

target "docker-crane" {
  dockerfile = "crane.dockerfile"
  target = "docker-crane"
  platforms = [
    "linux/amd64"
  ]
  cache-from = [
    "type=registry,ref=${CI_REGISTRY_IMAGE}/crane-cache:${CI_DEFAULT_BRANCH}",
    "type=registry,ref=${CI_REGISTRY_IMAGE}/crane-cache:${CI_COMMIT_REF_SLUG}"
  ]
  cache-to = [
    "type=registry,mode=max,image-manifest=true,oci-mediatypes=true,ref=${CI_REGISTRY_IMAGE}/crane-cache:${CI_COMMIT_REF_SLUG}"
  ]
  tags = [
    "${CI_REGISTRY_IMAGE}/docker-crane:${CI_COMMIT_REF_SLUG}",
    "${CI_REGISTRY_IMAGE}/docker-crane:${BUILD_HASH}"
  ]
  output = [
    "type=registry"
  ]
}

target "base" {
  dockerfile = "Dockerfile"
  target = "base"
  platforms = [
    "linux/amd64"
  ]
  cache-from = [
    "type=registry,ref=${CI_REGISTRY_IMAGE}/base-cache:${CI_DEFAULT_BRANCH}",
    "type=registry,ref=${CI_REGISTRY_IMAGE}/base-cache:${CI_COMMIT_REF_SLUG}"
  ]
  cache-to = [
    "type=registry,mode=max,image-manifest=true,oci-mediatypes=true,ref=${CI_REGISTRY_IMAGE}/base-cache:${CI_COMMIT_REF_SLUG}"
  ]
  tags = [
    "${CI_REGISTRY_IMAGE}/base:${BUILD_HASH}"
  ]
  output = [
    "type=registry"
  ]
}

target "app" {
  dockerfile = "Dockerfile"
  target = "app" 
  platforms = [
    "linux/amd64"
  ]
  cache-from = [
    "type=registry,ref=${CI_REGISTRY_IMAGE}/base-cache:${CI_DEFAULT_BRANCH}",
    "type=registry,ref=${CI_REGISTRY_IMAGE}/base-cache:${CI_COMMIT_REF_SLUG}",
    "type=registry,ref=${CI_REGISTRY_IMAGE}/app-cache:${CI_DEFAULT_BRANCH}",
    "type=registry,ref=${CI_REGISTRY_IMAGE}/app-cache:${CI_COMMIT_REF_SLUG}"
  ]
  cache-to = [
    "type=registry,mode=max,image-manifest=true,oci-mediatypes=true,ref=${CI_REGISTRY_IMAGE}/app-cache:${CI_COMMIT_REF_SLUG}"
  ]
  tags = [
    "${CI_REGISTRY_IMAGE}/gitlab-runner-orchestrator:${BUILD_HASH}"
  ]
  output = [
    "type=registry"
  ]
}