package src

import (
	"context"
	"errors"
	"fmt"
	"testing"

	"github.com/machinebox/graphql"
	"github.com/matryer/is"
	k8sclient "k8s.io/client-go/kubernetes"
)

type RunnerState struct {
	runners       []*RunnerData
	to_be_deleted []bool
	deleted       []bool
}

type mock_data struct {
	t       *testing.T
	cfg     RunnerConfig
	state   RunnerState
	pending int
}

func find_first(data []bool) (int, error) {
	for idx, x := range data {
		if x {
			return idx, nil
		}
	}
	return 0, errors.New("nothing found")
}

func find_runner_by_name(data []*RunnerData, name string) (int, error) {
	for idx, x := range data {
		if x.Name == name {
			return idx, nil
		}
	}
	return 0, errors.New("nothing found")
}

func find_runner_by_id(data []*RunnerData, id int) (int, error) {
	for idx, x := range data {
		if x.Id == id {
			return idx, nil
		}
	}
	return 0, errors.New("nothing found")
}

func (d *mock_data) scale_up_deployment(client *k8sclient.Clientset, ctx context.Context, namespace string, name string) error {
	idx, err := find_first(d.state.deleted)
	if err != nil {
		// if this fires we have a bug in the test not in the code
		// as we assume that len(d.state.runners) == d.cfg.Max
		d.t.Fatal("couldnt find runner to scale up")
	}
	if !d.state.deleted[idx] {
		// this probably indicates a bug in the mocks
		d.t.Fatal("trying to scale up the same pod twice")
	}
	d.state.deleted[idx] = false
	d.state.to_be_deleted[idx] = false
	d.state.runners[idx].Paused = false
	return nil
}

func (d *mock_data) scale_down_deployment(client *k8sclient.Clientset, ctx context.Context, namespace string, name string) error {
	idx, err := find_first(d.state.to_be_deleted)
	if err != nil {
		d.t.Fatal("scaled down deployment without marking pod first")
	}
	if d.state.deleted[idx] {
		// this probably indicates a bug in the mocks
		d.t.Fatal("trying to delete pod twice")
	}
	if !d.state.runners[idx].Paused {
		d.t.Fatal("trying to delete pod that is not paused")
	}
	d.state.deleted[idx] = true
	d.state.to_be_deleted[idx] = false
	d.state.runners[idx].Paused = false
	return nil
}

func (d *mock_data) mark_pod_for_deletion(client *k8sclient.Clientset, ctx context.Context, namespace string, name string) error {
	idx, err := find_runner_by_name(d.state.runners, name)
	if err != nil {
		d.t.Fatal("name not found")
	}
	if idx >= len(d.state.runners) {
		d.t.Fatal("idx is out of bound")
	}
	if d.state.to_be_deleted[idx] {
		// this probably indicates a bug in the mocks
		d.t.Fatal("trying to mark pod to be deleted twice")
	}
	d.state.to_be_deleted[idx] = true
	return nil
}

func (d *mock_data) runner_data(k8s *k8sclient.Clientset, gl *graphql.Client, ctx context.Context, cfg RunnerConfig, tags []string) ([]*RunnerData, error) {
	runners := make([]*RunnerData, len(d.state.runners))
	var scale = 0
	for idx, _ := range runners {
		if !d.state.deleted[idx] {
			runners[scale] = d.state.runners[idx]
			scale++
		}
	}
	return runners[:scale], nil
}

func (d *mock_data) pause(client *graphql.Client, ctx context.Context, runner_id int) error {
	idx, err := find_runner_by_id(d.state.runners, runner_id)
	if err != nil {
		d.t.Fatal("id not found")
	}
	if d.state.runners[idx].Paused {
		// this probably indicates a bug in the mocks
		d.t.Fatal("trying to pause twice")
	}
	d.state.runners[idx].Paused = true
	return nil
}

func (d *mock_data) unpause(client *graphql.Client, ctx context.Context, runner_id int) error {
	idx, err := find_runner_by_id(d.state.runners, runner_id)
	if err != nil {
		d.t.Fatal("id not found")
	}
	if !d.state.runners[idx].Paused {
		// this probably indicates a bug in the mocks
		d.t.Fatal("trying to unpause twice")
	}
	d.state.runners[idx].Paused = false
	return nil
}

func (d *mock_data) pending_per_tag(client *graphql.Client, ctx context.Context, tags []string) (int, error) {
	return d.pending, nil
}

func (d *mock_data) running_per_tag(client *graphql.Client, ctx context.Context, tags []string) (int, error) {
	var running = 0
	for idx, x := range d.state.runners {
		if !d.state.deleted[idx] {
			running += x.Running
		}
	}
	return running, nil
}

type params_as struct {
	cfg           RunnerConfig
	scale         int
	pending       int
	target_queue  int
	base_capacity int
}

type expect_as struct {
	err    bool
	active int
	scale  int
}

func test_config(t *testing.T, p *params_as) mock_data {
	runners := make([]*RunnerData, p.cfg.Max)
	to_be_deleted := make([]bool, p.cfg.Max)
	deleted := make([]bool, p.cfg.Max)
	for idx, _ := range runners {
		runners[idx] = &RunnerData{
			Id:      idx,
			Name:    fmt.Sprintf("%s-%d", p.cfg.Name, idx),
			Paused:  false,
			Running: 0,
		}
		to_be_deleted[idx] = false
		deleted[idx] = idx >= p.scale
	}
	state := RunnerState{
		runners:       runners,
		to_be_deleted: to_be_deleted,
		deleted:       deleted,
	}
	return mock_data{
		t:       t,
		cfg:     p.cfg,
		state:   state,
		pending: p.pending,
	}
}

func wrap_mock_data(d *mock_data) fnci {
	return fnci{
		scale_up_deployment:   d.scale_up_deployment,
		scale_down_deployment: d.scale_down_deployment,
		mark_pod_for_deletion: d.mark_pod_for_deletion,
		runner_data:           d.runner_data,
		pause:                 d.pause,
		unpause:               d.unpause,
		pending_per_tag:       d.pending_per_tag,
		running_per_tag:       d.running_per_tag,
	}
}

func test_autoscaler(t *testing.T, d mock_data, p params_as, e expect_as) {
	fnc := wrap_mock_data(&d)
	err := _run_autoscaler(
		&fnc,
		&Clients{},
		AutoScalerConfig{
			Tags:         []string{"generic"},
			TargetQueue:  p.target_queue,
			BaseCapacity: p.base_capacity,
			Runners:      []RunnerConfig{p.cfg},
		},
	)
	if !e.err && err != nil {
		t.Fatal(err)
	}
	if e.err && err == nil {
		t.Fatal("we expected an error")
	}
	var active = 0
	var scale = 0
	for idx := range d.state.runners {
		if !d.state.deleted[idx] {
			scale++
			if !d.state.runners[idx].Paused {
				active++
			}
		}
	}
	assert := is.New(t)
	assert.Equal(active, e.active)
	assert.Equal(scale, e.scale)
}

var basic_cfg = RunnerConfig{
	Name:       "gitlab-runner-generic-1",
	Namespace:  "gitlab-runner",
	Concurrent: 20,
	Min:        0,
	Max:        4,
}

func Test_autoscaler_no_change(t *testing.T) {
	p := params_as{
		cfg:           basic_cfg,
		scale:         0,
		pending:       0,
		target_queue:  0,
		base_capacity: 0,
	}
	d := test_config(t, &p)
	test_autoscaler(t, d, p,
		expect_as{
			err:    false,
			active: 0,
			scale:  0,
		},
	)
}

func Test_autoscaler_scale_up(t *testing.T) {
	p := params_as{
		cfg:           basic_cfg,
		scale:         0,
		pending:       5,
		target_queue:  0,
		base_capacity: 0,
	}
	d := test_config(t, &p)
	test_autoscaler(t, d, p,
		expect_as{
			err:    false,
			active: 1,
			scale:  1,
		},
	)
}

func Test_autoscaler_scales_up_multiple(t *testing.T) {
	p := params_as{
		cfg:           basic_cfg,
		scale:         0,
		pending:       75,
		target_queue:  0,
		base_capacity: 0,
	}
	d := test_config(t, &p)
	test_autoscaler(t, d, p,
		expect_as{
			err:    false,
			active: 4,
			scale:  4,
		},
	)
}

func Test_autoscaler_scale_down(t *testing.T) {
	p := params_as{
		cfg:           basic_cfg,
		scale:         1,
		pending:       0,
		target_queue:  0,
		base_capacity: 0,
	}
	d := test_config(t, &p)
	test_autoscaler(t, d, p,
		expect_as{
			err:    false,
			active: 0,
			scale:  1,
		},
	)
}

func Test_autoscaler_scales_down_multiple(t *testing.T) {
	p := params_as{
		cfg:           basic_cfg,
		scale:         4,
		pending:       0,
		target_queue:  0,
		base_capacity: 0,
	}
	d := test_config(t, &p)
	test_autoscaler(t, d, p,
		expect_as{
			err:    false,
			active: 0,
			scale:  4,
		},
	)
}

func Test_autoscaler_does_not_scale_down_if_there_are_pending_jobs(t *testing.T) {
	p := params_as{
		cfg:           basic_cfg,
		scale:         1,
		pending:       5,
		target_queue:  0,
		base_capacity: 0,
	}
	d := test_config(t, &p)
	test_autoscaler(t, d, p,
		expect_as{
			err:    false,
			active: 1,
			scale:  1,
		},
	)
}

func Test_autoscaler_does_only_scale_down_if_capacity_is_extraneous(t *testing.T) {
	p := params_as{
		cfg:           basic_cfg,
		scale:         1,
		pending:       0,
		target_queue:  0,
		base_capacity: 0,
	}
	d := test_config(t, &p)
	d.state.runners[0].Running = basic_cfg.Concurrent - 1
	test_autoscaler(t, d, p,
		expect_as{
			err:    false,
			active: 1,
			scale:  1,
		},
	)
}

func Test_autoscaler_scales_up_if_out_of_capacity(t *testing.T) {
	p := params_as{
		cfg:           basic_cfg,
		scale:         1,
		pending:       5,
		target_queue:  0,
		base_capacity: 0,
	}
	d := test_config(t, &p)
	d.state.runners[0].Running = basic_cfg.Concurrent
	test_autoscaler(t, d, p,
		expect_as{
			err:    false,
			active: 2,
			scale:  2,
		},
	)
}

func Test_autoscaler_does_not_scale_up_if_below_queue_depth(t *testing.T) {
	p := params_as{
		cfg:           basic_cfg,
		scale:         0,
		pending:       5,
		target_queue:  5,
		base_capacity: 0,
	}
	d := test_config(t, &p)
	d.state.runners[0].Running = basic_cfg.Concurrent
	test_autoscaler(t, d, p,
		expect_as{
			err:    false,
			active: 0,
			scale:  0,
		},
	)
}

func Test_autoscaler_does_not_scale_up_if_there_is_capacity_abroad(t *testing.T) {
	p := params_as{
		cfg:           basic_cfg,
		scale:         0,
		pending:       5,
		target_queue:  0,
		base_capacity: 5,
	}
	d := test_config(t, &p)
	d.state.runners[0].Running = basic_cfg.Concurrent
	test_autoscaler(t, d, p,
		expect_as{
			err:    false,
			active: 0,
			scale:  0,
		},
	)
}
