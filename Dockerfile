FROM golang:1.22.5-alpine3.20 AS base

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

FROM base AS build

COPY main.go ./
COPY src ./src

RUN go build .

FROM alpine:3.18 AS app

COPY --from=build /app/gitlab-runner-orchestrator /usr/local/bin