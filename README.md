# gitlab-runner-orchestrator

gitlab-runner-orchestrator is, as the name implies, the orchestrator you were looking for to keep your fleet of runners working together in harmony. It tries to automate all the tasks that the maintainers of a runner fleet need to perform and open up opportunities to conserve server usage.


## Components

The orchestrator is built as components, each of which solves one specific problem and can be activated independently.


### Reaper

When gitlab-runner restarts it loses track of all the jobs it is managing. The jobs themselves will not disappear until they are deleted by gitlab-runner though. The result is that the jobs will continue to linger around indefinitely in the cluster consuming resources and potentially even blocking new jobs from spawning. **Reaper** solves this issue by monitoring the jobs in the cluster, using a heuristic to determine whether the job is still running, and killing the jobs that are deemed finished or dead.

### Autoscaler

Gitlab-runner uses a polling protocol to retrieve jobs from gitlab, a runner therefore can't peek which jobs to pick up, if it requests a job it is expected to execute it too. This means that many optimizations, like extend to cloud to break the peak, or give priority to certain jobs is not possible to implement using that simple protocol. **Autoscaler** can work-around this limitation by querying the job queue using gitlab's graphql api instead, this allows it to control a fleet of runners implementing all these advanced behaviours. The most simple of which is horizontal autoscaling. The user can define a target queue depth and it will try to start and stop runners to match it. The implementation avoids common issues that would arise from autoscaling gitlab-runners using well-known operators. For example, runners are paused and depleted before they are scaled down, to avoid jobs lingering around indefinitely or failing to report success back to gitlab.


## Installation

### Get Repository Info

```bash
helm repo add gitlab-runner-orchestrator https://gitlab.com/api/v4/projects/55207189/packages/helm/stable
helm repo update
```

*See [helm repository](https://helm.sh/docs/helm/helm_repo/) for command documentation.*

### Install Chart

```bash
helm install [RELEASE_NAME] gitlab-runner-orchestrator/gitlab-runner-orchestrator
```

*See [helm install](https://helm.sh/docs/helm/helm_install/) for command documentation.*


## Configuration

Each component of gitlab-runner-orchestrator can be configured in detail using the a yaml configuration file. 

```yml
reaper:
  enabled: true
  interval_s: 600
  namespaces:
    - name: gitlab-runner
      deletion_delay_s: 300
      silence_threshold_s: 3600
autoscaler:
  enabled: true
  interval_s: 30
  groups:
    - tags: [gitlab-runner]
      target_queue: 0
      runners:
        - name: gitlab-runner-1
          namespace: gitlab-runner
          concurrent: 20
          min: 0
          max: 4
```