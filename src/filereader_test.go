package src

import (
	"io"
	"strings"
	"testing"
)

type params struct {
	text        string
	needle      string
	buffer_size int
}

type expect struct {
	match bool
	err   bool
}

func test(t *testing.T, p params, e expect) {
	fh := io.NopCloser(strings.NewReader(p.text))
	match, err := match_string_at_beginning_of_line(fh, p.needle, p.buffer_size)
	if !e.err && err != nil {
		t.Fatal(err)
	}
	if e.err && err == nil {
		t.Fatal("we expected an error")
	}
	if e.match && !match {
		t.Fatalf(`"%s" is in "%s"`, p.needle, p.text)
	}
	if !e.match && match {
		t.Fatalf(`"%s" is not in "%s"`, p.needle, p.text)
	}
}

func Test_match_on_first_line(t *testing.T) {
	test(t,
		params{
			text:        "this is a single line",
			needle:      "this",
			buffer_size: 1024,
		},
		expect{
			match: true,
			err:   false,
		},
	)
}

func Test_match_on_second_line(t *testing.T) {
	test(t,
		params{
			text:        "this is a the first line\nsecond line",
			needle:      "second",
			buffer_size: 1024,
		},
		expect{
			match: true,
			err:   false,
		},
	)
}

func Test_no_match(t *testing.T) {
	test(t,
		params{
			text:        "this is a the first line\nsecond line",
			needle:      "third",
			buffer_size: 1024,
		},
		expect{
			match: false,
			err:   false,
		},
	)
}

func Test_can_match_despite_line_too_long_for_buffer(t *testing.T) {
	test(t,
		params{
			text:        "this is a the first line\nsecond line",
			needle:      "second",
			buffer_size: len("second") + 1,
		},
		expect{
			match: true,
			err:   false,
		},
	)
}

func Test_if_buffer_cant_hold_needle_it_throws_an_error(t *testing.T) {
	test(t,
		params{
			text:        "this is a the first line\nsecond line",
			needle:      "second",
			buffer_size: len("second"),
		},
		expect{
			match: false,
			err:   true,
		},
	)
}

func Test_no_match_if_not_in_the_beginning_of_a_line(t *testing.T) {
	test(t,
		params{
			text:        "one two three",
			needle:      "two",
			buffer_size: len("one") + 1,
		},
		expect{
			match: false,
			err:   false,
		},
	)
}

type params2 struct {
	text string
}

type expect2 struct {
	empty bool
	err   bool
}

func test2(t *testing.T, p params2, e expect2) {
	fh := io.NopCloser(strings.NewReader(p.text))
	empty, err := is_empty(fh)
	if !e.err && err != nil {
		t.Fatal(err)
	}
	if e.err && err == nil {
		t.Fatal("we expected an error")
	}
	if e.empty && !empty {
		t.Fatalf(`"%s" is empty`, p.text)
	}
	if !e.empty && empty {
		t.Fatalf(`"%s" is not empty`, p.text)
	}
}

func Test_empty(t *testing.T) {
	test2(t,
		params2{
			text: "",
		},
		expect2{
			empty: true,
			err:   false,
		},
	)
}

func Test_non_empty(t *testing.T) {
	test2(t,
		params2{
			text: "asdf",
		},
		expect2{
			empty: false,
			err:   false,
		},
	)
}
