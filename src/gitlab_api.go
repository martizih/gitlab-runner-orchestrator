package src

import (
	"context"
	"fmt"
	"net/http"
	"regexp"
	"strconv"

	"github.com/machinebox/graphql"
	"github.com/pkg/errors"
)

type AuthorizationHeaderTransport struct {
	Transport http.RoundTripper
	Token     string
}

// RoundTrip adds an Authorization header to the request
func (t *AuthorizationHeaderTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	req.Header.Set("Authorization", "Bearer "+t.Token)
	return t.Transport.RoundTrip(req)
}

func Client(url string, token string) *graphql.Client {
	api_url := fmt.Sprintf("%s/api/graphql", url)
	httpClient := &http.Client{
		Transport: &AuthorizationHeaderTransport{
			Transport: http.DefaultTransport,
			Token:     token,
		},
	}
	return graphql.NewClient(api_url, graphql.WithHTTPClient(httpClient))
}

var runner_id_re = regexp.MustCompile(`^gid://gitlab/Ci::Runner/(\d+)$`)

var err_no_matching_runners_found = errors.New("no matching runners found")

func runner_by_name(client *graphql.Client, ctx context.Context, name string) (int, error) {
	type Node struct {
		ID          string `json:"id"`
		Description string `json:"description"`
	}
	type RunnerQueryResponse struct {
		Runners struct {
			Nodes []Node `json:"nodes"`
		} `json:"runners"`
	}

	req := graphql.NewRequest(`
		query RunnerByName($name: String) {
			runners(search: $name) {
				nodes {
					id
					description
				}
			}
		}
	`)
	req.Var("name", name)
	var resp RunnerQueryResponse
	err := client.Run(ctx, req, &resp)
	if err != nil {
		return 0, errors.Wrapf(err, "failed to query runner id by name for %s", name)
	}

	var data = make([]*Node, len(resp.Runners.Nodes))
	var idx = 0
	for _, x := range resp.Runners.Nodes {
		if x.Description == name {
			data[idx] = &x
			idx++
		}
	}
	nodes := data[:idx]

	if len(nodes) == 0 {
		return 0, err_no_matching_runners_found
	}
	if len(nodes) > 1 {
		return 0, errors.Errorf("more than one matching runners found for: %s", name)
	}
	runner := nodes[0].ID

	matches := runner_id_re.FindStringSubmatch(runner)
	if len(matches) != 2 {
		return 0, errors.Errorf("id format not understood")
	}

	id, err := strconv.Atoi(matches[1])

	if err != nil {
		return 0, errors.Wrap(err, "failed to convert to int")
	}

	return id, nil
}

type RunnerDetails struct {
	Description string   `json:"description"`
	TagList     []string `json:"taglist"`
	Paused      bool     `json:"paused"`
}

func runner_details(client *graphql.Client, ctx context.Context, runner_id int) (RunnerDetails, error) {
	type RunnerQueryResponse struct {
		Runner RunnerDetails `json:"runner"`
	}

	req := graphql.NewRequest(`
		query Runner($id: CiRunnerID!) {
			runner(id: $id){
				description
				tagList
				paused
			}
		}
	`)
	req.Var("id", fmt.Sprintf("gid://gitlab/Ci::Runner/%d", runner_id))
	var resp RunnerQueryResponse
	err := client.Run(ctx, req, &resp)
	if err != nil {
		return RunnerDetails{}, errors.Wrap(err, "failed to query runner details by id")
	}
	return resp.Runner, nil
}

func runner_jobcount(client *graphql.Client, ctx context.Context, runner_id int) (int, error) {
	type RunnerQueryResponse struct {
		Runner struct {
			Jobs struct {
				Count int `json:"count"`
			} `json:"jobs"`
		} `json:"runner"`
	}

	req := graphql.NewRequest(`
		query Runner($id: CiRunnerID!, $limit: Int) {
			runner(id: $id){
				jobs(statuses: RUNNING) {
					count(limit: $limit)
				}
			}
		}
	`)
	req.Var("id", fmt.Sprintf("gid://gitlab/Ci::Runner/%d", runner_id))
	limit := 1000
	req.Var("limit", limit)
	var resp RunnerQueryResponse
	err := client.Run(ctx, req, &resp)
	if err != nil {
		return 0, errors.Wrap(err, "failed to query jobcount")
	}
	if resp.Runner.Jobs.Count == limit+1 {
		fmt.Println("query count exceeds limit", limit)
	}
	return resp.Runner.Jobs.Count, nil
}

func memoize(fn func(*graphql.Client, context.Context, int) (string, error)) func(*graphql.Client, context.Context, int) (string, error) {
	cache := make(map[int]string)
	inner := func(client *graphql.Client, ctx context.Context, project_id int) (string, error) {
		v, ok := cache[project_id]
		if !ok {
			v, err := fn(client, ctx, project_id)
			if err != nil {
				return "", err
			}
			cache[project_id] = v
		}
		return v, nil
	}
	return inner
}

func _project_by_id(client *graphql.Client, ctx context.Context, project_id int) (string, error) {
	type ProjectQueryResponse struct {
		Projects struct {
			Nodes []struct {
				FullPath string `json:"fullPath"`
			} `json:"nodes"`
		} `json:"projects"`
	}

	req := graphql.NewRequest(`
		query Project($id: [ID!]) {
			projects(ids: $id) {
				nodes {
					fullPath
				}
			}
	  	}
	`)
	req.Var("id", fmt.Sprintf("gid://gitlab/Project/%d", project_id))

	var resp ProjectQueryResponse
	err := client.Run(ctx, req, &resp)
	if err != nil {
		return "", errors.Wrap(err, "failed to query project by id")
	}
	return resp.Projects.Nodes[0].FullPath, nil
}

var project_by_id = memoize(_project_by_id)

func job_status(client *graphql.Client, ctx context.Context, project string, job_id int) (string, error) {
	type JobQueryResponse struct {
		Project struct {
			Job struct {
				Status string `json:"status"`
			} `json:"job"`
		} `json:"project"`
	}

	req := graphql.NewRequest(`
		query Job($fullPath: ID!, $id: JobID!) {
			project(fullPath: $fullPath) {
				job(id: $id) {
					status
				}
			}
		}
	`)
	req.Var("fullPath", project)
	req.Var("id", fmt.Sprintf("gid://gitlab/Ci::Build/%d", job_id))
	var resp JobQueryResponse
	err := client.Run(ctx, req, &resp)
	if err != nil {
		return "", errors.Wrap(err, "failed to query job status")
	}
	return resp.Project.Job.Status, nil
}

func _status(client *graphql.Client, ctx context.Context, status string) (int, error) {
	type JobQueryResponse struct {
		Jobs struct {
			Count int `json:"count"`
		} `json:"jobs"`
	}

	req := graphql.NewRequest(`
		query Jobs($statuses: [CiJobStatus!], $limit: Int) {
			jobs(statuses: $statuses) {
				count(limit: $limit)
			}
		}
	`)
	req.Var("statuses", status)
	limit := 1000
	req.Var("limit", limit)
	var resp JobQueryResponse
	err := client.Run(ctx, req, &resp)
	if err != nil {
		return 0, errors.Wrap(err, "failed to query job count")
	}
	if resp.Jobs.Count == limit+1 {
		fmt.Println("query count exceeds limit", limit)
	}
	return resp.Jobs.Count, nil
}

func running(client *graphql.Client, ctx context.Context) (int, error) {
	return _status(client, ctx, "RUNNING")
}

func pending(client *graphql.Client, ctx context.Context) (int, error) {
	return _status(client, ctx, "PENDING")
}

func contains(haystack []string, needle string) bool {
	for _, a := range haystack {
		if a == needle {
			return true
		}
	}
	return false
}

func is_subset(as []string, bs []string) bool {
	for _, a := range as {
		if contains(bs, a) {
			continue
		}
		return false
	}
	return true
}

func is_equivalent(as []string, bs []string) bool {
	return is_subset(as, bs) && is_subset(bs, as)
}

func _jobs_per_tags(client *graphql.Client, ctx context.Context, status string, tags []string) (int, error) {
	type JobQueryResponse struct {
		Jobs struct {
			Nodes []struct {
				Tags []string `json:"tags"`
			} `json:"nodes"`
			PageInfo struct {
				HasNextPage bool   `json:"hasNextPage"`
				EndCursor   string `json:"endCursor"`
			} `json:"pageInfo"`
		} `json:"jobs"`
	}

	req := graphql.NewRequest(`
		query Jobs($statuses: [CiJobStatus!], $after: String) {
			jobs(statuses: $statuses, after: $after) {
				nodes {
					tags
				}
				pageInfo {
					hasNextPage
					endCursor
				}
			}
		}
	`)

	var hasNextPage = true
	var after = ""
	count := 0
	for hasNextPage {
		req.Var("statuses", status)
		req.Var("after", after)
		var resp JobQueryResponse
		err := client.Run(ctx, req, &resp)
		if err != nil {
			return 0, errors.Wrap(err, "failed to query jobs")
		}

		for _, job := range resp.Jobs.Nodes {
			if len(job.Tags) > 0 && is_subset(job.Tags, tags) {
				count++
			}
		}
		hasNextPage = resp.Jobs.PageInfo.HasNextPage
		after = resp.Jobs.PageInfo.EndCursor
	}

	return count, nil
}

func running_per_tag(client *graphql.Client, ctx context.Context, tags []string) (int, error) {
	return _jobs_per_tags(client, ctx, "RUNNING", tags)
}

func pending_per_tag(client *graphql.Client, ctx context.Context, tags []string) (int, error) {
	return _jobs_per_tags(client, ctx, "PENDING", tags)
}

func _pause(client *graphql.Client, ctx context.Context, runner_id int, paused bool) error {
	req := graphql.NewRequest(`
		mutation PauseRunner($id: CiRunnerID!, $paused: Boolean) {
			runnerUpdate(input: {
				id: $id
				paused: $paused
			}) {
				errors
			}
		}
	`)
	req.Var("id", fmt.Sprintf("gid://gitlab/Ci::Runner/%d", runner_id))
	req.Var("paused", paused)
	err := client.Run(ctx, req, nil)
	if err != nil {
		return errors.Wrap(err, "failed to pause/unpause runner")
	}
	return nil
}

func pause(client *graphql.Client, ctx context.Context, runner_id int) error {
	return _pause(client, ctx, runner_id, true)
}

func unpause(client *graphql.Client, ctx context.Context, runner_id int) error {
	return _pause(client, ctx, runner_id, false)
}
