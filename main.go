package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	src "martizih/gitlab-runner-orchestrator/src"

	"github.com/pkg/errors"
)

type stackTracer interface {
	StackTrace() errors.StackTrace
}

func _main() error {
	config, err := src.LoadConfig("config.yaml")
	if err != nil {
		return err
	}

	err = src.VerifyConfig(config.AutoScaler.Groups)
	if err != nil {
		return err
	}

	url, success1 := os.LookupEnv("GITLAB_URL")
	token, success2 := os.LookupEnv("GITLAB_TOKEN")

	if !success1 || !success2 {
		return errors.New("Required variables not defined")
	}

	c := src.Client(url, token)
	ctx := context.Background()

	client, err := src.ConnectToK8s()
	if err != nil {
		return err
	}

	clients := src.Clients{
		K8S: client,
		GL:  c,
		CTX: ctx,
	}

	err1 := make(chan error)
	var started = 0
	if config.Reaper.Enabled {
		go func() {
			fmt.Println("Reaper: starting")
			for {
				err := src.RunReaper(&clients, config.Reaper.Namespaces, err1)
				if err != nil {
					err1 <- err
				}
				fmt.Println("Reaper: going to sleep", config.Reaper.IntervalS)
				time.Sleep(time.Duration(config.Reaper.IntervalS) * time.Second)
			}
		}()
		started++
	}

	err2 := make(chan error)
	if config.AutoScaler.Enabled {
		go func() {
			fmt.Println("Autoscaler: starting")
			for {
				err := src.RunAutoscaler(&clients, config.AutoScaler.Groups)
				if err != nil {
					err2 <- err
				}
				fmt.Println("Autoscaler: going to sleep", config.AutoScaler.IntervalS)
				time.Sleep(time.Duration(config.AutoScaler.IntervalS) * time.Second)
			}
		}()
		started++
	}

	if started > 0 {
		select {
		case err := <-err1:
			return err
		case err := <-err2:
			return err
		}
	}
	return nil
}

func main() {
	err := _main()
	if err != nil {
		st, ok := err.(stackTracer)
		if ok {
			fmt.Printf("%+v\n", st)
		}
		log.Fatal("Main:", err)
	}
}
