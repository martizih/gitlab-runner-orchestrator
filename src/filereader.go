package src

import (
	"bufio"
	"bytes"
	"io"
	"strings"

	_ "unsafe"

	"github.com/pkg/errors"
)

//go:linkname dropCR bufio.dropCR
func dropCR(data []byte) []byte

func match_string_at_beginning_of_line(file io.ReadCloser, needle string, maxScanTokenSize int) (bool, error) {
	if maxScanTokenSize < len(needle)+1 {
		return false, errors.New("buffersize not sufficient to hold needle")
	}

	newline := true
	split := func(data []byte, atEOF bool) (advance int, token []byte, err error) {
		if atEOF && len(data) == 0 {
			return 0, nil, nil
		} else if atEOF {
			// If we're at EOF, we have a final, non-terminated line. Return it.
			return len(data), dropCR(data), nil
		} else if i := bytes.IndexByte(data, '\n'); i >= 0 {
			// We have a full newline-terminated line.
			// signal that the next read is at the beginning of a line
			newline = true
			return i + 1, dropCR(data[0:i]), nil
		} else if len(data) < len(needle)+1 {
			// Request more data.
			return 0, nil, nil
		} else {
			// return whatever we have now
			// signal that the next read is not at the beginning of a line
			newline = false
			return len(data), dropCR(data), nil
		}
	}

	scanner := bufio.NewScanner(file)
	scanner.Buffer(make([]byte, maxScanTokenSize), maxScanTokenSize)
	scanner.Split(split)

	nextline_is_newline := newline
	for scanner.Scan() {
		line := scanner.Text()
		if nextline_is_newline && strings.HasPrefix(line, needle) {
			return true, nil
		}
		nextline_is_newline = newline
	}
	err := scanner.Err()
	if err != nil {
		return false, errors.Wrap(err, "failed run through job log")
	}
	return false, nil
}

func is_empty(file io.ReadCloser) (bool, error) {
	limitedReader := &io.LimitedReader{R: file, N: 100}
	// ReadAll allocates make([]byte, 0, 512)
	data, err := io.ReadAll(limitedReader)
	if err != nil {
		return false, err
	}
	return len(data) == 0, nil
}
