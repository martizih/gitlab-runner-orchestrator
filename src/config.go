package src

import (
	"os"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
)

type Config struct {
	Reaper struct {
		Enabled    bool           `yaml:"enabled"`
		IntervalS  int            `yaml:"interval_s"`
		Namespaces []ReaperConfig `yaml:"namespaces"`
	} `yaml:"reaper"`
	AutoScaler struct {
		Enabled   bool               `yaml:"enabled"`
		IntervalS int                `yaml:"interval_s"`
		Groups    []AutoScalerConfig `yaml:"groups"`
	}
}

func LoadConfig(path string) (*Config, error) {
	file, err := os.ReadFile(path)
	if err != nil {
		return nil, errors.Wrap(err, "failed to load config file")
	}

	var config Config
	err = yaml.Unmarshal(file, &config)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse config file")
	}

	return &config, nil
}
