package src

import (
	"context"
	"fmt"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	k8sclient "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

func ConnectToK8s() (*k8sclient.Clientset, error) {
	config, err := rest.InClusterConfig()
	if err != nil {
		return nil, errors.Wrap(err, "failed to get kubeconfig")
	}
	return kubernetes.NewForConfig(config)
}

func DebugConnectToK8s() (*k8sclient.Clientset, error) {
	kubeconfig := filepath.Join(
		os.Getenv("HOME"), ".kube", "config",
	)
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get kubeconfig")
	}
	return kubernetes.NewForConfig(config)
}

func pods_in_deployment(client *k8sclient.Clientset, ctx context.Context, namespace string, name string) (*v1.PodList, error) {
	pods, err := client.CoreV1().Pods(namespace).List(ctx, metav1.ListOptions{
		LabelSelector: fmt.Sprintf("app=%s", name),
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to list pods in deployment")

	}
	return pods, nil
}

func scale_up_deployment(client *k8sclient.Clientset, ctx context.Context, namespace string, name string) error {
	deploymentClient := client.AppsV1().Deployments(namespace)

	deployment, err := deploymentClient.Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		return errors.Wrap(err, "failed to get pod in deployment")
	}

	(*deployment.Spec.Replicas)++

	_, err = deploymentClient.Update(ctx, deployment, metav1.UpdateOptions{})
	if err != nil {
		return errors.Wrap(err, "failed to update deployment")
	}
	return nil
}

func scale_down_deployment(client *k8sclient.Clientset, ctx context.Context, namespace string, name string) error {
	deploymentClient := client.AppsV1().Deployments(namespace)

	deployment, err := deploymentClient.Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		return errors.Wrap(err, "failed to get pod in deployment")
	}

	(*deployment.Spec.Replicas)--

	_, err = deploymentClient.Update(ctx, deployment, metav1.UpdateOptions{})
	if err != nil {
		return errors.Wrap(err, "failed to update deployment")
	}
	return nil
}

func mark_pod_for_deletion(client *k8sclient.Clientset, ctx context.Context, namespace string, name string) error {
	err := mark_pod(client, ctx, namespace, name, "controller.kubernetes.io/pod-deletion-cost", "-1")
	if err != nil {
		return err
	}
	return mark_pod(client, ctx, namespace, name, "controller.gitlab-runner-orchestrator.io/deleted", "true")
}

func mark_pod(client *k8sclient.Clientset, ctx context.Context, namespace string, name string, key string, value string) error {
	pod, err := client.CoreV1().Pods(namespace).Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		return errors.Wrap(err, "failed to get pod in namespace")
	}

	// Add or update the annotation
	if pod.Annotations == nil {
		pod.Annotations = make(map[string]string)
	}
	pod.Annotations[key] = value

	_, err = client.CoreV1().Pods(namespace).Update(ctx, pod, metav1.UpdateOptions{})
	if err != nil {
		return errors.Wrap(err, "failed to update pod")
	}
	return nil
}

func get_mark(client *k8sclient.Clientset, ctx context.Context, namespace string, name string, key string) (bool, string, error) {
	pod, err := client.CoreV1().Pods(namespace).Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		return false, "", errors.Wrap(err, "failed to get pod in namespace")
	}

	if pod.Annotations == nil {
		return false, "", nil
	}
	val, exists := pod.Annotations[key]
	return exists, val, nil
}

func kill_pod(client *k8sclient.Clientset, ctx context.Context, namespace string, name string) error {
	err := client.CoreV1().Pods(namespace).Delete(ctx, name, metav1.DeleteOptions{})
	if err != nil {
		return errors.Wrap(err, "failed to delete pod")
	}
	return nil
}
